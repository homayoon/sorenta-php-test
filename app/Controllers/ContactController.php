<?php

namespace App\Controllers;

use App\Models\Contact;
use App\Services\Response;

class ContactController
{
    public function show()
    {
        if (!isset($_GET['id'])){
            echo Response::jsonResponse('not fount', 404); die;
        }

        $contact = Contact::find($_GET['id']);
        echo Response::jsonResponse($contact, 200); die;
    }

    public function create()
    {
        if (!isset($_POST['frm'])){
            echo Response::jsonResponse('', 400); die;
        }

        $contact = Contact::create($_POST['frm']);
        echo Response::jsonResponse($contact, 200); die;
    }

    public function update()
    {
        if (!isset($_POST['id'])){
            echo Response::jsonResponse('not fount', 404); die;
        }

        if (!isset($_POST['frm'])){
            echo Response::jsonResponse('not fount', 404); die;
        }

        $contact = Contact::update($_POST['id'], $_POST['frm']);
        echo Response::jsonResponse($contact, 200); die;
    }

}
