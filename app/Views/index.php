<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <title>Sorenta PHP AJAX Test</title>
</head>
<body>
<form method="post" id="create_form" class="row align-items-end justify-content-center mt-5">
    <div class="col-auto">
        <label for="firstname">First Name:</label>
        <input class="form-control" type="text" id="firstname" name="frm[firstname]" placeholder="First Name">
    </div>
    <div class="col-auto">
        <label for="lastname">Last Name:</label>
        <input class="form-control" type="text" id="lastname" name="frm[lastname]" placeholder="Last Name">
    </div>
    <div class="col-auto">
        <label for="address">Address:</label>
        <input class="form-control" type="text" id="address" name="frm[address]" placeholder="Address">
    </div>
    <div class="col-auto">
        <button class="btn btn-primary" type="button" id="btn_submit">Add +</button>
        <button class="btn btn-success d-none" type="button" id="btn_update">Update</button>
        <button class="btn btn-secondary d-none" type="button" id="btn_cancel">Cancel</button>
    </div>
    <input type="hidden" name="id" id="id">
</form>
<p class="text-danger text-center" id="error_text"></p>
<hr>

<table class="table">
    <thead>
    <tr>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Address</th>
        <th scope="col">Created at</th>
        <th scope="col">Updated at</th>
        <th scope="col"></th>
    </tr>
    </thead>

    <tbody id="table_body">
    <!--  Handle Show Items From Db  -->
    <?php
    foreach (App\Models\Contact::all() as $row) {
        echo "<tr id='item_{$row->id}'>
               <td>{$row->firstname}</td>
               <td>{$row->lastname}</td>
               <td>{$row->address}</td>
               <td>{$row->created_at}</td>
               <td>{$row->updated_at}</td>
               <td><button class='btn btn-link' type='button' onclick='showEditItemForm(event)' id='{$row->id}'>Edit</button></td>
            </tr>";
    }
    ?>
    </tbody>
</table>
<script>
    let firstnameInput = document.getElementById('firstname');
    let lastnameInput = document.getElementById('lastname');
    let addressInput = document.getElementById('address');
    let idInput = document.getElementById('id');

    let myHeaders = new Headers();
    myHeaders.append('pragma', 'no-cache');
    myHeaders.append('cache-control', 'no-cache');

    const handleSubmitForm = (event) => {
        event.preventDefault();
        let formElement = document.getElementById('create_form');
        let formData = new FormData(formElement)
        let errorTextElem = document.getElementById('error_text');
        errorTextElem.innerText = '';
        fetch('index.php?controller=ContactController&action=create', {
            method: 'POST',
            body: formData,
            headers: myHeaders,
        })
            .then(response => response.json())
            .then(data => {
                let {message, status} = data;
                if (!status) {
                    errorTextElem.innerText = message
                    return;
                }
                let elem = createTableElement(message);
                console.log(elem)
                document.getElementById('table_body').append(elem)
                formElement.reset();
            })
            .catch((error) => {
                console.log('Error:', error);
            });
    }

    const handleUpdateItem = (event) => {
        event.preventDefault();
        let formElement = document.getElementById('create_form');
        let formData = new FormData(formElement)
        let errorTextElem = document.getElementById('error_text');
        errorTextElem.innerText = '';
        fetch('index.php?controller=ContactController&action=update', {
            method: 'POST',
            body: formData,
            headers: myHeaders,
        })
            .then(response => response.json())
            .then(data => {
                let {message, status} = data;
                if (!status) {
                    errorTextElem.innerText = message
                    return;
                }
                document.getElementById('item_'+message.id).replaceWith(createTableElement(message))
                formElement.reset();
            })
            .catch((error) => {
                console.log('Error:', error);
            });
    }

    const createTableElement = (contact) => {
        let inner = '<td>' + contact.firstname + '</td>' +
            '<td>' + contact.lastname + '</td>' +
            '<td>' + contact.address + '</td>'+
            '<td>' + contact.created_at + '</td>'+
            '<td>' + contact.updated_at + '</td>'+
            '<td><button class="btn btn-link" type="button" onclick="showEditItemForm(event)" id='+contact.id+'>Edit</button></td>'

        let row = document.createElement('tr');
        row.innerHTML = inner
        row.setAttribute('id','item_'+contact.id)
        return row;
    }

    const showEditItemForm = (event) => {
        event.preventDefault();
        document.getElementById("btn_update").classList.remove('d-none')
        document.getElementById("btn_cancel").classList.remove('d-none')
        document.getElementById("btn_submit").classList.add('d-none')

        document.getElementById('error_text').innerText = '';
        fetch(`index.php?controller=ContactController&action=show&id=${event.target.getAttribute('id')}`, {
            method: 'GET',
            headers: myHeaders,
        })
            .then(response => response.json())
            .then(data => {
                let {message, status} = data;

                firstnameInput.value = message.firstname
                lastnameInput.value = message.lastname
                addressInput.value = message.address
                idInput.value = message.id

            })
            .catch((error) => {
                console.log('Error:', error);
            });
    }

    const clearEditForm = () => {
        document.getElementById("btn_update").classList.add('d-none')
        document.getElementById("btn_cancel").classList.add('d-none')
        document.getElementById("btn_submit").classList.remove('d-none')
        document.getElementById('create_form').reset();
    }

    // Event Handler For Form Submit
    document.getElementById("btn_submit").addEventListener('click', handleSubmitForm);
    document.getElementById("btn_update").addEventListener('click', handleUpdateItem);
    document.getElementById("btn_cancel").addEventListener('click', clearEditForm);
</script>
</body>
</html>
