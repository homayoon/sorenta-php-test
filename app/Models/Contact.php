<?php
namespace App\Models;

use App\Db;

class Contact
{
    public $id;
    public $firstname;
    public $lastname;
    public $address;
    public $created_at;
    public $updated_at;

    public function __construct($contact)
    {
        $this->id = $contact['id'];
        $this->firstname = $contact['firstname'];
        $this->lastname = $contact['lastname'];
        $this->address = $contact['address'];
        $this->created_at = $contact['created_at'];
        $this->updated_at = $contact['updated_at'];
    }

    public static function all()
    {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT * FROM contacts');

        foreach ($req->fetchAll() as $contact) {
            $list[] = new Contact($contact);
        }

        return $list;
    }

    public static function find($id)
    {
        $db = Db::getInstance();
        $id = intval($id);
        $req = $db->prepare('SELECT * FROM contacts WHERE id = :id');
        $req->execute(['id' => $id]);
        $contact = $req->fetch();
        return new Contact($contact);
    }

    public static function create($data)
    {
        $db = Db::getInstance();
        $req = $db->prepare("INSERT INTO contacts (firstname, lastname, address) VALUES ('$data[firstname]','$data[lastname]','$data[address]')");
        $req->execute();
        $req = $db->query('SELECT * FROM contacts ORDER BY id DESC LIMIT 1');
        $contact = $req->fetch();
        return new Contact($contact);
    }

    public static function update($id, $data)
    {
        $db = Db::getInstance();
        $req = $db->prepare("UPDATE contacts SET firstname = :firstname, lastname = :lastname, address = :address WHERE id = :id");
        $data['id'] = intval($id);
        $req->execute($data);
        $req = $db->query('SELECT * FROM contacts ORDER BY id DESC LIMIT 1');
        $contact = $req->fetch();
        return new Contact($contact);
    }


}
