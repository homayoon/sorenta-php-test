<?php
use App\controllers\ContactController;

function call($controller, $action)
{
    require_once('controllers/' . $controller . '.php');

    switch ($controller) {
        case 'ContactController':
            $controller = new ContactController();
            break;
    }
    $controller->{$action}();
}

// we're adding an entry for the new controller and its actions
$controllers = [
    'ContactController' => ['index', 'show', 'create', 'update']
];
if (isset($_REQUEST['controller']) && array_key_exists($_REQUEST['controller'], $controllers)) {
    if (in_array($_REQUEST['action'], $controllers[$_REQUEST['controller']])) {
        call($_REQUEST['controller'], $_REQUEST['action']);
    }
}
