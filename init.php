<?php

// Create database
try {
    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
    $conn = new PDO('mysql:host=localhost', 'root', '', $pdo_options);
    $sql = "CREATE DATABASE php_mvc";
    $conn->query($sql);
    $conn = null;
} catch (Exception $e) {
    echo $e->getMessage();
    exit();
}
//Create table
try {
    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
    $conn = new PDO('mysql:host=localhost;dbname=php_mvc', 'root', '', $pdo_options);
    $sql = "CREATE TABLE contacts (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(255) NOT NULL,
        lastname VARCHAR(255) NOT NULL,
        address VARCHAR(255),
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";

    $conn->exec($sql);
} catch (PDOException $e) {
    echo $e->getMessage();
    exit();
}
echo 'Database And Tables Created Successfully!';
